# Friend Invite

You can create a friend invite in discord with the API, but not the client. These will display correctly in the client as follows:
![Image of friend invite](./assets/friend_invite.png)
![Image of another friend invite](./assets/friend_invite2.png)

## POST https://discord.com/api/v9/users/@me/invites
No payloads necessary

## Response
Response code: 200
| **Property** | **Type** | **Description**                                                                        |
|--------------|----------|----------|
| channel      | null     | null   |
| code         | string   | ID for the invite. Can be accessed with discord.gg/{code} or discord.com/invite/{code} |
| created_at   | Date     | Timestamp at which the invite was created  |
| expires_at   | Date     | Timestamp at which the invite will expire. Cannot be customized   |
| inviter      | [User Object](https://discord.com/developers/docs/resources/user#user-object)   | User who created the invite     |
| max_age      | 604800   | Time before invite expires (in seconds). Cannot be customized    |
| max_uses     | 5        | Max amount of uses for the link. Cannot be customized  |
| type         | 2        | Unknown   |
| uses         | number   | The amount of uses the invite has  |